we need to do an exercise to identify positive behaviour and negative behaviour for the 3 values mentioned by Robert H(CCO)
Here are the 3 values mentioned by Robert H

1.Dedication to the client success
 	+ve: While targeting our aim ,we should  keep this thought in mind that its not only about fulfilling the targets rather its more about 
  	     making something which could prove to be fruitful for the client and which could take them up to the next level.
 	-ve: Giving client's improvement and it's betterment a second thought, while focussing just on completing the targets.
2.Reciprocity
	+:Giving the client the best of our work in responds to their trust on us.
	-:Showing a self centered attittude.
3.Trust ourselves and others
	+:Having the believe in our self that we could achieve the targets, along with helping our peers to think the same 
  	  by letting them know their strengths and helping them overcome their weaknesses.
	-:Demotivating ourself,and Criticizing peers for their inability and weaknesses








package com.springboot.mapping;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

 @Bean
 public Docket postsApi() {
  return new Docket(DocumentationType.SWAGGER_2).groupName("public-api").apiInfo(apiInfo()).select()
    .paths(postPaths()).build();
 }

 private Predicate<String> postPaths() {
  return or(regex("/api/posts.*"), regex("/api/manage.*"));
 }

 private ApiInfo apiInfo() {
  return new ApiInfoBuilder().title("Manager API").description("Manager API reference for developers")
    .termsOfServiceUrl("http://manager.com").description("manager.com").license("Manager License")
    .licenseUrl("sdfgdsg@gmail.com").version("1.0").build();
 }

}





<dependency>
   <groupId>io.springfox</groupId>
   <artifactId>springfox-swagger2</artifactId>
   <version>2.4.0</version>
  </dependency>

  <dependency>
   <groupId>io.springfox</groupId>
   <artifactId>springfox-swagger-ui</artifactId>
   <version>2.4.0</version>
  </dependency>





http://localhost:8095/swagger-ui.html#!/manage-controller/allManagersUsingGET




  public static Specification<CustomerArInformationArinfo> customerARInformationLogicalArinfol0(
      int companyNumber, int customerDistrictNumber, int aRCustomerNumber) {
    return (root, cq, cb) -> {
      cq.orderBy(Arrays.asList(cb.asc(root.get("companyNumber")),
          cb.asc(root.get("customerDistrictNumber")), cb.asc(root.get("aRCustomerNumber"))));

      List<Predicate> predicates = new ArrayList<Predicate>();

      if (companyNumber > 0) {
        predicates.add(cb.greaterThanOrEqualTo(root.get("companyNumber"), companyNumber));
      }

      if (customerDistrictNumber > 0) {
        predicates.add(
            cb.greaterThanOrEqualTo(root.get("customerDistrictNumber"), customerDistrictNumber));
      }

      if (aRCustomerNumber > 0) {
        predicates.add(cb.greaterThanOrEqualTo(root.get("aRCustomerNumber"), aRCustomerNumber));
      }

      return cb.and(predicates.toArray(new Predicate[0]));
    };
  }









Pageable pageable=new PageRequest(0,10);
  List<SalestaxFileSalestax> fileSalestaxs = (List<SalestaxFileSalestax>) fileSalestaxRepository
    .findAll(SalestaxFileSalestaxSpecification.salestaxfileFI930CLSpecification(),pageable);









##SDHOME
##GETEXH
AR031
##GETFLW
AR035
AR036
AR032
RAR02
RAR32
SAR410
AR120CL
AR120PCL
AR442X
AR420C
AR446
LN310
SAR430
ARP12
AR417














